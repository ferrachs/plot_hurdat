#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later
# -*- coding: utf-8 -*-


'''
Sylvaine Ferrachat 2019-07

Various custom parser settings
'''

#------------------------------------------
#-- Import preamble

import re

#------------------------------------------
#-- Decorate begin's create_parser function
#   to set an epilog

def deco_create_parser(func, epilog=None):
    def wrap_create_parser(*args, **kwargs):
        parser = func(*args, **kwargs)
        parser.epilog = epilog
        return parser
    return wrap_create_parser

       
#-----------------------------------------------------
#-- Decorate argparse HelpFormatter's option's helpers

def deco_argparse_format_action(func, is_invoc=False):
    def wrapper_format_action(*args, **kwargs):
        '''
        Replaces `-f FOO' or  '--foo FOO` by resp. `-f=FOO` and `--foo=FOO` in argparse helper.
        Although there is always possibility to use either `--foo FOO` or
        `--foo=FOO`, it is sometimes better to document the '=' way
        rather than the ' ' way.
        For example, when the option argument contains special characters like '-'
        it must be expressed with '=' in order to not confuse the shell that executes
        the command line.
        '''
        string = func(*args, **kwargs)
        if is_invoc is True:
            # 'invocation section' case
            patt = r"(-+[^ ,]+) +"
            repl = r"\1="
        else:
            # 'usage section' case
            patt = r"(\[-+[^ \]]+) +([^|])"
            repl = r"\1=\2"

        return re.sub(patt,repl,string)
    return wrapper_format_action

