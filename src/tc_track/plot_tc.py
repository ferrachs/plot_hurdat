#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later
# -*- coding: utf-8 -*-


'''
Sylvaine Ferrachat 2019-04

Run `plot_tc.py --help` for full description / usage
or inspect the below `run` function docstring
'''

#------------------------------------------
#-- Import preamble

#--- Python distrib
from argparse import HelpFormatter
from collections import OrderedDict
from datetime import datetime
import itertools
import logging
from pathlib import Path
import sys
import textwrap

#--- Custom
import begin
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
from tc_track import custom_parser
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.patches as mpatches
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.colors import ListedColormap, BoundaryNorm
from matplotlib.collections import LineCollection
import numpy as np

#------------------------------------------
#-- Constants / defaults

DFT = {
    'cat': ('TD','TS','TC1','TC2','TC3','TC4','TC5'),
    'wind_max': (33,63,82,95,112,135,1000),
    'bnd_yr': (1850,2100),
    'bnd_lon': (-180,180),
    'bnd_lat': (-90,90),
    'sel_storms': None,
    'inc_lon': None,
    'inc_lat': None,
    'proj': 'PlateCarree',
    #'proj_arg' : dict(central_longitude=180),
    'central_lon' : 0,
    'title': None,
    'title_fontsize': None,
    'legend_fontsize': None,
    'cmap': 'jet',
    'show_mk': True,
    'legend_ncol': 1,
    'legend_loc': 'upper right',
    'legend_mk_scale': 1.25,
    'cbar_fontsize': None,
    'mk_symb': (
        'o', 'v', '^', '<', '>', 
        's', 'p', 'P', '*', 'H',
        'X', 'D', 'd',
        ),
    'mk_face_col': (
        (1,1,1), (.4,.4,.4),
        ),
    'mk_edge_col': (
        'k', (.5,.5,.5),
        ),
    'mk_size': 50,
    'mk_lsty':('-',':'),
    'beg_end_labels':('Begin','End'),
    'outfile': None,
    'text_width' : 80,

    } 

DFT['bnd_cat'] = (DFT['cat'][0], DFT['cat'][-1])
DFT['bnd_cat_idx'] = [DFT['cat'].index(s) for s in DFT['bnd_cat']]

storm_cat = [(x[0],x[1]) for x in zip(DFT['cat'],DFT['wind_max'])]

mk_props = ('edge_col', 'face_col', 'symb')
#------------------------------------------
#-- Functions

def maxwind_to_cat(wind):
    '''
    Find storm category from max wind values 
    '''
    for ca,wi in zip(DFT['cat'],DFT['wind_max']):
        if wind <= wi:
            return ca
    return 'unknown category'

def cat_to_idx(label):
    '''
    Return storm category index from its label
    '''
    try:
        return DFT['cat'].index(label)
    except ValueError:
        if label == '':
            return None
        else:
            msg = (
                    f"Invalid storm category '{label}'.\n"
                    f"Possible values: {DFT['cat']}"
                    )
            logging.error(msg)
            sys.exit(1)

    
def to_float(s=str):
    '''
    Convert string to float if possible
    '''
    try:
        return float(s)
    except ValueError:
        pass

def to_int(s=str):
    '''
    Convert string to int if possible
    '''
    try:
        return int(s)
    except ValueError:
        pass

def fix_backslash(s=str):
    '''
    Circumvent pbs with shell inserting '\' in front of any '\'
    '''
    #return s.encode('utf-8').decode('unicode_escape') #works also, but more obscure to my taste...
    return s.replace('\\n','\n')

def in_range(v,bnd):
    '''
    Return True/False if a value is in a specified range
    '''
    #SF ToDo: exclude one of the 2 bounds for consistency?
    in_range = True
    if v < bnd[0] or v > bnd[1]:
        in_range = False
    return in_range

def fact_sanitize_rng(bnd, is_cat=False, is_lon=False):
    def sanitize_rng(string=str):
        '''
        Sanitize all range options
        '''
        
        #-- Parse cmdline entry
        if is_cat is not True:
            rng = [to_float(s) for s in string.split(',')]
        else:
            rng = [cat_to_idx(s) for s in string.split(',')]

        #-- Be resilient to singleton entries
        if len(rng) == 1:
            rng.append(rng[0])

        #-- Attempt to sort (may not be always relevant)
        try:
            if not is_lon:
                rng.sort()
        except TypeError:
            pass

        #-- Special case of longitudes: converts to -180,180 if it necessary
        # ToDo (maybe generalize and allow 'N/W/S/E' values too...)

        #-- Cleanup None and not-in-range values
        for i,v in enumerate(rng):
            if v is None or (not in_range(v,bnd) and not is_lon):
                rng[i] = bnd[i]

        #-- Sort again
        if not is_lon:
            rng.sort

        return rng
    return sanitize_rng

def sanitize_sel_storms(string):

    return [s.strip().upper() for s in string.split(',')]

def sanitize_kwargs(string=str):
    '''
    Sanitize kwargs-like options 
    (ie strings entered as "a=1,b=2,...")
    '''
    kwargs = dict(
            [(k,to_float(v)) for x in string.split(',')
               for (k,v) in [x.split('=')] ]
            )
    return kwargs

def sanitize_infile_type(string:str):
    #ToDo: cleanup (quick and dirty for now)
    string = string.lower()
    if string in ['hurdat', 'rsmc']:
        if string == 'hurdat':
            return Hurdat
        else:
            return Rsmc
    else:
        msg = f"Error! Unknown input file type '{string}'!"
        logging.error(msg)
        sys.exit(1)

def fix_2_digit_year(yy):
    current_year = datetime.now().strftime('%y')
    century = 2000
    if yy > current_year:
        #-- Assuming yy is in the past and in 20th century:
        century = 1900

    if type(yy) in [int,float]:
        return century+yy
    elif type(yy) is str:
        return f"{str(century)[0:2]}{yy}"
    else:
        msg = f"Unsupported type for year value '{yy}'"
        logging.error(msg)
        sys.exit(1)

#------------------------------------------
#-- Classes

class Storm:

    def __init__(self, 
                 years = DFT['bnd_yr'], maxcat = DFT['bnd_cat_idx'], sel_storms=DFT['sel_storms']):

        self.years = years
        self.maxcat = maxcat
        self.sel_storms = sel_storms

    def _set_cat(self):
        for k in self.all_storms.keys():
            self.all_storms[k]['data']['cat'] = [maxwind_to_cat(x) 
                                            for x in self.all_storms[k]['data']['max_wind_speed']]
        return self

    def _select_storms(self):

        selected_storms = {}
        for k,storm in self.all_storms.items():
            max_cat_idx = max([cat_to_idx(s) for s in storm['data']['cat']])
            #-- Store max cat in object for convenience:
            self.all_storms[k]['max_cat'] = DFT['cat'][max_cat_idx] 

            #-- Check if storm conforms to conditions
            year_ok      = in_range(float(storm['year']),self.years)
            maxcat_ok    = in_range(max_cat_idx,self.maxcat)
            if self.sel_storms is None:
                sel_storms_ok = True
            else:
                sel_storms_ok = (storm['label'] in self.sel_storms) \
                             or (storm['name']  in self.sel_storms)
            
            #-- Add storm if it fulfills user-defined conditions
            cond = [year_ok, maxcat_ok, sel_storms_ok]
            if all(cond) is True:
                selected_storms[k] = storm

        #-- Check for empty 'storms' data structure:
        if len(selected_storms) == 0:
            logging.warning('No storm fulfills user-defined conditions. Exiting...')
            sys.exit(0)

        return selected_storms

    def process(self):
        self._set_cat()
        self.storms = self._select_storms() 

    def list_all_storms(self):

        print(f"\nList of all storms in the database:")
        print(f"  -----------------------------------")
        for storm in self.all_storms.values():
            print(f"    - {storm['label']}")
        print("\n")
        return self

class Hurdat(Storm):

    def __init__(self,infile:Path,*supargs, **supkwargs):

        self.data_crs = ccrs.PlateCarree()
        self.infile = infile
        self.all_storms = self._read()
        super().__init__(*supargs, **supkwargs)
        self.process()

    @staticmethod
    def to_degrees(s:str):
        c = s[-1]
        if c in ('N','E'):
            v = float(s[0:-1])
        elif c in ('S'):
            v = -float(s[0:-1])
        elif c in ('W'):
            v = 360-float(s[0:-1])
        else:
            v = float(s)
        return v

    def _read(self):

        sep = ','
        info_cols = {'ID':0,'name':1,'rec_len':2}
        data_cols = {
            'date':{
                'col':0,
                'func':lambda x : datetime.strptime(x,'%Y%m%d'),
                },
             'lat':{
                'col':4,
                'func':self.to_degrees,
                },
              'lon':{
                'col':5,
                'func':self.to_degrees,
                },
              'max_wind_speed':{
                'col':6,
                'func':lambda x : float(x),
                },
              }
              
        with open(self.infile,mode='r') as f:
            lines = f.readlines()
            raw_data = [line.split(sep) for line in lines]
            
        all_storms = {}
       
        idx = 0
        while idx < len(lines):

            #-- Read storm header
            h = raw_data[idx]
            d = {}
            for k,v in info_cols.items():
                d[k] = h[v].strip()
            d['year'] = d['ID'][4:]
            d['rec_len'] = int(d['rec_len'])
            
            storm_id = d['ID']
            
            # Define 'label' for convenience
            d['label'] = d['name'] + ' ' + d['year']
    
            idx += 1
            iend = idx+d['rec_len']
            
            data = {}
            for k,v in data_cols.items():
                col  = v['col']
                func = v['func']
                data[k] = [func(x[col]) for x in raw_data[idx:iend]]
                
            d['data'] = data
            all_storms[storm_id] = d
            idx += d['rec_len']
        return all_storms

class Rsmc(Storm):

    def __init__(self,infile:Path,*supargs, **supkwargs):

        self.data_crs = ccrs.PlateCarree()
        self.infile = infile
        self.all_storms = self._read()
        super().__init__(*supargs, **supkwargs)
        self.cure_missing_wind_entries()
        self.process()

    def _read(self):
        info_cols = {'ID':(6,10),'name':(30,50),'rec_len':(12,15)}
        data_cols = {
              'date':{
                  'chars':(0,8),
                  'func': lambda x : datetime.strptime(fix_2_digit_year(x),'%Y%m%d%H'),
                  },
              'grade':{
                  'chars':(13,14),
                  'func': lambda x : int(x),
                  },
              'lat':{
                  'chars':(15,18),
                  'func' : lambda x : float(x)*0.1,
                  },
              'lon':{
                  'chars':(19,23),
                  'func' : lambda x : float(x)*0.1,
                  },
              'max_wind_speed':{
                  'chars':(33,36),
                  'func': lambda x : float(x) if x != '' else None,
                  },
              }

        with open(self.infile,mode='r') as f:
            lines = f.readlines()
            
        all_storms = {}
       
        idx = 0
        while idx < len(lines):

            #-- Read storm header
            h = lines[idx]
            d = {}
            for k,v in info_cols.items():
                d[k] = h[v[0]:v[1]].strip()

            d['year'] = fix_2_digit_year(d['ID'][0:2])
            d['rec_len'] = int(d['rec_len'])
            if d['name'] == '':
                d['name'] = 'UNNAMED'
            
            storm_id = d['ID']
            
            # Define 'label' for convenience
            d['label'] = d['name'] + ' ' + d['year']
    
            idx += 1
            iend = idx+d['rec_len']
            
            data = {}
            for k,v in data_cols.items():
                beg = v['chars'][0]
                end = v['chars'][1]
                f = v['func']
                data[k] = [f(line[beg:end].strip()) for line in lines[idx:iend]]
                
            d['data'] = data
            all_storms[storm_id] = d
            idx += d['rec_len']
        return all_storms

    def cure_missing_wind_entries(self):

        # Temporary solution: exclude all entries where wind is missing
        rem_storms = []
        for k,storm in self.all_storms.items():
            if any([(x is None) for x in storm['data']['max_wind_speed']]):
                rem_storms.append(k)

        for k in rem_storms:
            self.all_storms.pop(k)

        return self

#------------------------------------------
def set_fig(figsize=(10,8),
            title=DFT['title'],
            title_fontsize=DFT['title_fontsize'],
            proj=ccrs.PlateCarree(),
            ):

    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(1, 1, 1, projection = proj)
    if title is not None:
        text = fig.suptitle(title,
                x=0.5,
                y=1.025,
                verticalalignment='baseline',
                transform=ax.transAxes)
        if title_fontsize is not None:
            text.set_size(title_fontsize)

    return fig, ax

def set_locator(lim,inc,is_lon=False):

    rng = np.arange(lim[0],lim[1]+inc,inc)
    #-- Fix 0-360 -> -180,180 range for longitudes
    #   I don't quite understand why, but 0-360 is not well handled by
    #   cartopy when the plot projection is centered on lon=180.
    if is_lon is True:
        loc = np.array([x-360 if x > 180 else x for x in rng])
    else:
        loc = np.array([x for x in rng])

    return loc

def set_map(ax,
        plot_proj,
        lim_lon=DFT['bnd_lon'],
        lim_lat=DFT['bnd_lat'],
        inc_lon=DFT['inc_lon'],
        inc_lat=DFT['inc_lat'],
        ):

    # Note that even though there's an optional kwarg 'proj', the whole script wouldn't currently
    # support anything else than 'PlateCarree'. --> ToDo if necessary.

    data_crs = ccrs.PlateCarree()

    ax.set_global()
    ax.add_feature(cfeature.LAND)
    ax.add_feature(cfeature.OCEAN)
    #ax.add_feature(cfeature.COASTLINE)
    #ax.add_feature(cfeature.BORDERS, linestyle=':')
    ax.add_feature(cfeature.LAKES, alpha=0.5)
    ax.add_feature(cfeature.RIVERS)

    #-- Set limits, only if this is a PlateCarree instance
    #   otherwise it may create pbs
    #SF202302: maybe no pb anymore --> ToDo: to check 
    if isinstance(plot_proj,ccrs.PlateCarree):
        ax.set_extent([*lim_lon, *lim_lat], crs=data_crs)

    #-- Set gridlines if possible
    try:
        kwargs = {
                'linewidth':0.5,
                'color':'gray',
                'alpha':0.5,
                'linestyle':'--',
                }
        if isinstance(plot_proj,ccrs.PlateCarree):
            kwargs['draw_labels'] = True

        if inc_lon is not None:
            xlocs = set_locator(lim_lon,inc_lon,is_lon=True)
        else:
            xlocs = None 

        if inc_lat is not None:
            ylocs = set_locator(lim_lat,inc_lat)
        else:
            ylocs = None 

        gl = ax.gridlines(**kwargs,
                          xlocs = xlocs,
                          ylocs = ylocs,
                          crs=data_crs,
                          )

        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
    except TypeError:
        # unsupported gridlines for requested projection
        pass

    return ax

def plot_storms(ax,fig,data_crs,storms,
        colormap=DFT['cmap'],
        cbar_fontsize=DFT['cbar_fontsize'],
        show_mk=DFT['show_mk'],
        mk_size=DFT['mk_size'],
        mk_lsty=DFT['mk_lsty'],
        legend_ncol=DFT['legend_ncol'],
        legend_loc=DFT['legend_loc'],
        legend_fontsize=DFT['legend_fontsize'],
        legend_mk_scale=DFT['legend_mk_scale'],
        ):

    #-- Set colorscale
    cmap = plt.get_cmap(colormap, len(DFT['cat']))
    norm = BoundaryNorm(np.arange(len(DFT['cat'])+1)-0.5,len(DFT['cat']))

    #-- Prepare marker properties
    mk_iter = itertools.cycle(itertools.product(DFT['mk_edge_col'],DFT['mk_face_col'],DFT['mk_symb']))

    for storm in storms.values():
        logging.info(f"Plotting {storm['label']} (Max cat: {storm['max_cat']})")

        data = storm['data']

        #-- Prepare for creating segments between each event 
        #   (1 segment  = 1 TC intensity)
        x = data['lon']
        y = data['lat']
        p0 = [(xx,yy) for xx,yy in zip(x[:-1],y[:-1])]
        p1 = [(xx,yy) for xx,yy in zip(x[1:],y[1:])]

        #-- Make segments and fix periodic boundary issues
        #   by splitting the cross-border segments
        #   into 2 pieces
        minlon=0.
        maxlon=360.
        li = []
        for start,stop in zip(p0,p1):
            #-- detect if segments goes across boundaries
            crossed = (start[0]-maxlon*0.5)*(stop[0]-maxlon*0.5) < 0
            #if crossed is False:
            if (start[0]-maxlon*0.5)*(stop[0]-maxlon*0.5) >= 0 \
                    or \
                    abs(stop[0]-start[0]) <= (maxlon-minlon)*0.5 :
                # standard case:
                li.append((start,stop))
            else:
                # border is crossed
                f = lambda x,y: (x+maxlon,y) if x<=maxlon*0.5 else (x,y)
                fake_start = f(*start)
                fake_stop = f(*stop)
                pti = []
                ptiy = (maxlon-fake_start[0])*(fake_stop[1]-fake_start[1])/(fake_stop[0]-fake_start[0]) + fake_start[1]
                if start[0]>= maxlon*0.5:
                    li.append((start,(maxlon,ptiy)))
                    li.append(((minlon,ptiy),stop))
                else:
                    li.append((start,(minlon,ptiy)))
                    li.append(((maxlon,ptiy),stop))

        segments = np.array(li)

        lc = LineCollection(segments, cmap=cmap, norm=norm, transform=data_crs)
        lc.set_array(np.array([DFT['cat'].index(string) for string in data['cat']]))
        line = ax.add_collection(lc)

        #-- Add markers
        if show_mk is True:
            marker = dict(zip(mk_props,next(mk_iter)))
            for idx in (0,-1):
                if idx == 0:
                    label = storm['label']
                else:
                    label = None

                ax.scatter(x[idx],y[idx],color='None',
                    marker=marker['symb'],
                    linestyle=mk_lsty[idx],
                    s=mk_size,
                    edgecolor=marker['edge_col'],
                    facecolor=marker['face_col'],
                    label=label,
                    transform=data_crs,
                    zorder=100,
                    )

    #-- Draw legend if relevant:
    if show_mk:

        lgd_kwargs = {
            'ncol':legend_ncol,
            'markerscale':legend_mk_scale,
            'loc':legend_loc,
            'handletextpad':0.05,
            }
        if legend_fontsize:
            lgd_kwargs['fontsize']=legend_fontsize

        h,l = ax.get_legend_handles_labels()

        #-- Add extra legend items for beginning and end of storm
        #   only if symbols for beg and end *are* different
        if len(set(mk_lsty)) > 1:

            #-- Add 'separator' between normal legend items and the following ones
            h.append(mpatches.Patch(edgecolor='none',facecolor='none',linestyle='-',label=' '))
    
            #-- Add 'begin' and 'end' legends as extra patches
            for idx,prop in enumerate(zip(DFT['beg_end_labels'],mk_lsty)):
                label = prop[0]
                lsty = prop[1]
                h.append(mpatches.Patch(
                    edgecolor=DFT['mk_edge_col'][0],
                    facecolor=DFT['mk_face_col'][0],
                    linestyle=lsty,
                    label=label,
                    ))

        leg = plt.legend(handles=h,**lgd_kwargs)

        #-- Reduce patch width (default is too large)
        fact = 0.65 
        for patch in leg.get_patches():
            width = patch.get_width()
            patch.set_width(width*fact)
            #-- Center again the patch
            patch.set_x((1-fact)*width*0.5)

        #-- Set patch item fontstyle 
        for t in leg.get_texts():
            if t.get_text() in DFT['beg_end_labels']:
                t.set_fontproperties(mpl.font_manager.FontProperties(style='italic'))
                t.set_fontstyle('italic')

    #-- Prepare for colorbar:
    #   get position of main graph first, and create axis based on it
    divider = make_axes_locatable(ax)
    ax_cb = divider.new_vertical(size="5%", pad=0.4, axes_class=plt.Axes, pack_start=True)
    # See above axes_class trick in:
    # https://stackoverflow.com/questions/30030328/correct-placement-of-colorbar-relative-to-geo-axes-cartopy
    fig.add_axes(ax_cb)

    #-- Add colorbar (from last 'line' object) with using previously created axis (cax)
    cbar = fig.colorbar(line, ax=ax, cax=ax_cb, 
                        ticks=[DFT['cat'].index(string) for string in DFT['cat']], orientation = 'horizontal')

    #-- Fine-tune colorbar
    cbar.ax.set_xticklabels(DFT['cat'])
    cbxtick_obj = plt.getp(cbar.ax.axes, 'xticklines')
    plt.setp(cbxtick_obj, color='none')

    if cbar_fontsize:
        cbar.ax.tick_params(labelsize=cbar_fontsize)

    return ax,fig

#------------------------------------------
#-- Main

years_helper = '''
Select storms by year. It may be a single value or a comma-separated range, e.g. --years=1978,2000.
Please read the below section 'RANGE NOTATION' for full details on how to specify a year range.
'''
maxcat_helper = '''
Select storms by maximum category reached. It may be a single value or a comma-separated
range, e.g. --maxcat=TC3,TC5. In the latter case, it means that all storms which have reached TC3, TC4 or TC5 will be plotted.
Please read the below section 'RANGE NOTATION' for full details on how to specify a 'maxcat' range.
'''

proj_helper='''
Define map projection. For help on possible projections, please review https://scitools.org.uk/cartopy/docs/latest/crs/projections.html.
'''

proj_arg_helper = '''
Define arguments for the chosen cartopy projection, with syntax --proj-arg='arg1=val1,arg2=val2,...'. For help on possible arguments, please review https://scitools.org.uk/cartopy/docs/latest/crs/projections.html.
For example to set the central longitude to 180E, use:
--proj-arg='central_longitude=180'
'''
central_lon_helper = '''
Set the central longitude for the specified projection (e.g. 0 or 180 to show either Atlantic or Pacific areas.). 
'''

lon_helper = '''
Select longitude range to plot, expressed with the (-180,180) convention. It may be a single value or a comma-separated range, e.g. --lon=-120,10. 
Please read the below section 'RANGE NOTATION' for full details on how to specify a 'lon' range.
'''

lat_helper = '''
Select latitude range to plot, expressed with the (-90,90) convention. It may be a single value or a  comma-separated range, e.g. --lat=-80,10.
Please read the below section 'RANGE NOTATION' for full details on how to specify a 'lat' range.
'''

sel_storms_helper = '''
Select storms by name or by label (label == "name YYYY"). Multiple values need to be comma-separated. Example: --sel-storms="wilma 2005,andrew". Name and labels are case insensitive.
'''

inc_lon_helper = '''
Longitude gridline increment (None means "automatically chosen").
'''

inc_lat_helper = '''
latitude gridline increment (None means "automatically chosen").
'''

show_mk_helper = '''
Show markers and their legend (may be slow and cluttered when many tropical cyclones are shown)
'''

mk_size_helper = '''
Set marker size.
'''
mk_beg_lsty_helper = '''
Define line style for the 'storm beginning' marker (possible values: '-',':','--','-.')
'''

mk_end_lsty_helper = '''
Define line style for the 'storm ending' marker (possible values: '-',':','--','-.')
'''
colormap_helper = '''
Define colormap. For help on possible colormap names, see
https://matplotlib.org/users/colormaps.html
'''

cbar_fontsize_helper = '''
Define colorbar fontsize (None means "default from matplotlib").
'''

title_fontsize_helper = '''
Define plot title fontsize (None means "default from matplotlib").
'''

legend_fontsize_helper = '''
Define plot legend fontsize (None means "default from matplotlib").
'''

legend_mk_scale_helper = '''
Define marker scale factor in legend as compared to plot.
'''

legend_loc_helper = '''
Define the location of the legend. For help on possible locations, see
https://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.legend
with the restriction that only strings like 'lower right' or 'center' will be accepted
(no tuple).
'''

outfile_helper = '''
Plot file name. If omitted, the plot will be directly shown in an independent window.
'''

infile_helper = '''
Input file (so far supporting HURDAT2 (https://www.nhc.noaa.gov/data/#hurdat) or RSMC (https://www.jma.go.jp/jma/jma-eng/jma-center/rsmc-hp-pub-eg/besttrack.html) formats).
'''

infile_type_helper = '''
Type of the dataset: either 'hurdat' or 'rsmc' (case insensitive).
'''

section_title = '''

---------------
RANGE NOTATION:
---------------
'''

parag1 = textwrap.fill(
'''Options accepting a range as argument may comprise either a singleton or a comma-separated range.
Incomplete ranges, e.g. values like '--some-option=,2000' or '--some-option=1990,' are also accepted. The missing bound will be replaced by the min/max possible value from the data.
''',DFT['text_width'])

parag2 = textwrap.fill(
'''!!WARNING!! A singleton value (e.g. '--some-option=2000') is equivalent to '--some-option=2000,2000' and should not be confused with '--some-option=2000,'.
''', DFT['text_width'])

section_config_title = '''

--------------------------
Using a configuration file
--------------------------
'''

parag3 = textwrap.fill(
'''To avoid a lengthy command line with a lot of options, you may use a configuration file with presets for some of the option values. This file should be named `plot_tc.cfg` and located in your current directory. It will then be automatically read. It should be formatted the following way:''',DFT['text_width'])

parag4 = '''
[run]
<option 1> = value
<option 2> = value
...

Note that any line starting with '#' will be ignored.

Example of a valid configuration file:

[run]
sel_storms = GNE 1938, Mitch 1998, Juan 2003
mk_size = 100
legend_ncol=4
'''

parag5 = textwrap.fill(
'''If an option is defined both in the config file and in the command line, the command line value will have precedence. To disable an entire configuration file, best is to renamed it.
''',DFT['text_width'])

epilog = section_title +  parag1 + '\n\n' + parag2 + '\n\n' \
        + section_config_title + parag3 + '\n\n' + parag4 + '\n\n' + parag5 + '\n\n'

#-- Handle parser's epilog
begin.cmdline.create_parser = custom_parser.deco_create_parser(
        begin.cmdline.create_parser,
        epilog=epilog,
        )

#-- Customize helper of options with arguments
#   (print '-f=FOO, --foo=FOO' instead of '-f FOO, --foo FOO')
HelpFormatter._format_action_invocation = custom_parser.deco_argparse_format_action(
        HelpFormatter._format_action_invocation,
        is_invoc = True,
        )

HelpFormatter._format_actions_usage = custom_parser.deco_argparse_format_action(
        HelpFormatter._format_actions_usage,
        )

#-- Define formatter's class
formatter_class = begin.formatters.compose(
        begin.formatters.RawDescription,
        )

@begin.start(cmd_delim='--',
             config_file='plot_tc.cfg',
             short_args=False,
             formatter_class=formatter_class,
            )

@begin.convert(
        years  = fact_sanitize_rng(DFT['bnd_yr']),
        maxcat = fact_sanitize_rng(DFT['bnd_cat_idx'], is_cat=True),
        #proj_arg = sanitize_kwargs,
        lon    = fact_sanitize_rng(DFT['bnd_lon'], is_lon=True), 
        lat    = fact_sanitize_rng(DFT['bnd_lat']), 
        sel_storms = sanitize_sel_storms,
        inc_lon = to_float,
        inc_lat = to_float,
        legend_ncol = to_int,
        mk_size = to_int,
        legend_mk_scale = to_int,
        title = fix_backslash,
        infile_type = sanitize_infile_type,
        central_lon = to_int,
        )

@begin.logging

def run(
    infile: infile_helper,
    infile_type: infile_type_helper = Hurdat,
    years: years_helper = DFT['bnd_yr'],
    maxcat: maxcat_helper = DFT['bnd_cat'],
    proj : proj_helper = DFT['proj'],
    #>>SF disabling this feature for now
    # (--> simpler central_lon option)
    #proj_arg : proj_arg_helper = DFT['proj_arg'],
    #<<SF
    central_lon :central_lon_helper = DFT['central_lon'],
    lon: lon_helper = DFT['bnd_lon'],
    lat: lat_helper = DFT['bnd_lat'],
    sel_storms: sel_storms_helper = DFT['sel_storms'],
    inc_lon: inc_lon_helper = DFT['inc_lon'],
    inc_lat: inc_lat_helper = DFT['inc_lat'],
    colormap: colormap_helper = DFT['cmap'],
    cbar_fontsize: cbar_fontsize_helper = DFT['cbar_fontsize'],
    title: 'Define plot title' = DFT['title'],
    title_fontsize: title_fontsize_helper = DFT['title_fontsize'],
    show_mk: show_mk_helper = DFT['show_mk'],
    mk_size: mk_size_helper = DFT['mk_size'],
    mk_beg_lsty: mk_beg_lsty_helper = DFT['mk_lsty'][0],
    mk_end_lsty: mk_end_lsty_helper = DFT['mk_lsty'][1],
    legend_ncol: 'Number of columns to be displayed in legend' = DFT['legend_ncol'],
    legend_loc: legend_loc_helper = DFT['legend_loc'],
    legend_fontsize: legend_fontsize_helper = DFT['legend_fontsize'],
    legend_mk_scale: legend_mk_scale_helper = DFT['legend_mk_scale'],
    outfile: outfile_helper = DFT['outfile'],
       ):

    '''
    Plot storm tracks from either:
        * the hurdat2 database (https://www.nhc.noaa.gov/data/#hurdat)
        * or the RSMC database (https://www.jma.go.jp/jma/jma-eng/jma-center/rsmc-hp-pub-eg/besttrack.html)
    '''

    #-- Set projection
    #proj = getattr(ccrs,proj)(**proj_arg)
    proj = getattr(ccrs,proj)(central_longitude=central_lon)

    #-- Prepare figure and map
    fig, ax = set_fig(figsize=(12,8), title=title, title_fontsize=title_fontsize, proj=proj)
    ax = set_map(ax, plot_proj=proj, lim_lon=lon, lim_lat=lat, inc_lon=inc_lon, inc_lat=inc_lat)

    #-- Security: if default maxcat, convert labels to indices 
    #   This is necessary because default values are not automatically sanitized, in contrast
    #   to user-defined ones. Besides, the default value need to be of string type, to get it
    #   correct in the helper.
    if maxcat == DFT['bnd_cat']:
         maxcat = DFT['bnd_cat_idx']

    #-- Read and filter out storms
    cls = infile_type
    db_storms = cls(infile,
                    years = years,
                    maxcat = maxcat,
                    sel_storms = sel_storms)

    storms = db_storms.storms

    #-- Plot storms
    ax,fig = plot_storms(ax,fig, db_storms.data_crs, storms,
            show_mk = show_mk,
            mk_lsty = (mk_beg_lsty,mk_end_lsty),
            legend_ncol = legend_ncol,
            legend_loc = legend_loc,
            legend_fontsize = legend_fontsize,
            legend_mk_scale = legend_mk_scale,
            colormap = colormap,
            cbar_fontsize = cbar_fontsize,
            mk_size=mk_size,
            )

    #-- Finalize:
    if outfile is not None:
        fig.savefig(outfile)
    else:
        plt.show()

    return
